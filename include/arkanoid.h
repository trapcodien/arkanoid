/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arkanoid.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:38:42 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 22:23:07 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARKANOID_H
# define ARKANOID_H

# include <sys/types.h>
# include <dirent.h>
# include <string.h>
# include <errno.h>

# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <GLFW/glfw3.h>

# include "libft.h"
# include "arkanoid_struct.h"

extern t_game	g_ame;

/*
** ft_update.c
*/
void			ft_render(GLFWwindow *window);
void			ft_update(GLFWwindow *window);

/*
** ft_loader.c
*/
t_bool			load_blockfile(char *filename);
void			load_maps(void);
t_bool			select_map(void);

/*
** ft_block.c
*/
void			add_blockline(char *line, int line_num);
void			draw_blocks(void);
void			erase_blocks(void);

/*
** ft_maths.c
*/
float			ft_cosinus(float x);
float			ft_sinus(float x);

/*
** ft_collide.c
*/
void			ft_collision_pad_ball(void);
void			ft_collision_block_ball(void);

/*
** ft_draw.c
*/
void			draw_ball(void);
void			draw_block(t_block *block);

/*
** ft_init.c
*/
t_bool			game_init(void);
void			ft_reset_ball(void);

/*
** ft_score.c
*/
void			print_digit(int position, char digit);
void			draw_score(void);

/*
** ft_life.c
*/
void			draw_life(void);

/*
** ft_segments.c
*/
void			print_segments(int position, char *state);
void			print_segment_a(int position);
void			print_segment_b(int position);
void			print_segment_c(int position);
void			print_segment_d(int position);

/*
** ft_segments_extended.c
*/
void			print_segment_e(int position);
void			print_segment_f(int position);
void			print_segment_g(int position);

#endif
