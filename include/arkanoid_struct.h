/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arkanoid_struct.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:38:45 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 16:38:46 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ARKANOID_STRUCT_H
# define ARKANOID_STRUCT_H

# define PI		3.14159
# define TAU	(PI * 2)
# define WIDTH	1024
# define HEIGHT	768

typedef struct		s_elem
{
	struct s_elem	*next;
	struct s_elem	*prev;
	float			x1;
	float			y1;
	float			x2;
	float			y2;
	int				d;
}					t_elem;

typedef struct		s_ball
{
	float			x1;
	float			y1;
	float			x2;
	float			y2;
	float			dir_x;
	float			dir_y;
	float			radius;
	float			speed;
}					t_ball;

typedef t_elem		t_block;

typedef struct		s_map
{
	struct s_map	*next;
	struct s_map	*prev;
	t_block			*data;
	int				nb_blocks;
}					t_map;

typedef struct		s_game
{
	t_map			*maps;
	t_block			*blocks;
	int				nb_blocks;
	t_ball			ball;
	t_block			pad;
	int				score;
	int				lives;
	t_bool			left;
	t_bool			right;
	float			ratio;
}					t_game;

#endif
