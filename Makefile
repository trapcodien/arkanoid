#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jvincent <jvincent@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/01/16 10:40:18 by jvincent          #+#    #+#              #
##   Updated: 2015/05/03 21:55:20 by garm             ###   ########.fr       ##
#                                                                              #
#******************************************************************************#

RESET = 		\x1b[0m
RED = 			\x1b[33;01m
GREEN = 		\x1b[32;01m

CC =			clang

NAME =			arkanoid

FLAGS =			-Wall -Wextra -Werror \
				-I./include -I ./libft/includes \

LDFLAGS = 		-L libft -lft -L lib -lglfw3 -framework Cocoa \
				-framework OpenGL -framework IOKit -framework CoreVideo

LIBRARY :=		lib/libglfw3.a
LIBFT :=		libft/libft.a

INC_DIR =		include/
SRC_DIR = 		sources/

SRC_FILES =		main.c \
				ft_block.c \
				ft_loader.c \
				ft_maths.c \
				ft_collide.c \
				ft_init.c \
				ft_draw.c \
				ft_score.c \
				ft_life.c \
				ft_segments.c \
				ft_segments_extended.c \
				ft_update.c \

INC_FILES =		arkanoid.h \
				arkanoid_struct.h \

SRC =			$(addprefix $(SRC_DIR), $(SRC_FILES))
INC =			$(addprefix $(INC_DIR), $(INC_FILES))
OBJ =			$(SRC:.c=.o)

all: $(NAME)

%.o: %.c $(INC)
	@$(CC) $(FLAGS) -o $@ -c $<

$(NAME): $(LIBFT) $(LIBRARY) $(OBJ)
	@$(CC) $(FLAGS) -o $(NAME) $(OBJ) $(LDFLAGS)
	@echo "[$(GREEN)COMPILED$(RESET)] $(NAME)"

$(LIBRARY):
	@git submodule init; 
	@git submodule update; 
	@cd lib/glfw && cmake -DCMAKE_INSTALL_PREFIX:PATH=`pwd`/../.. . && make install;

$(LIBFT):
	@make -C libft

clean:
	@echo "[$(RED)cleaning...$(RESET)]"

	@if [ -f lib/glfw/Makefile ] ; \
	then \
		make clean -C lib/glfw ; \
	fi;

	@echo "[$(RED)deleted$(RESET)] GLFW Objects"
	@rm -f $(OBJ)
	@make clean -C libft
	@echo "[$(RED)deleted$(RESET)] Objects"

cleanbin:
	@if [ -f lib/glfw/install_manifest.txt ] ; \
	then \
		make uninstall -C lib/glfw ; \
	fi;

	@rm -rf lib/cmake
	@rm -rf lib/pkgconfig
	@rm -rf lib/glfw/install_manifest.txt
	@rm -rf include/GLFW
	@make cleanbin -C libft
	@rm -f $(NAME)

fclean: clean cleanbin
	@echo "[$(RED)deleted$(RESET)] Binary"

re: fclean all

.SILENT:

.PHONY: lib clean fclean re all
