/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 20:31:31 by garm              #+#    #+#             */
/*   Updated: 2014/06/16 23:56:48 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_putendl(const char *str)
{
	int		ret;

	ret = write(STDOUT_FILENO, str, ft_strlen(str));
	ft_putchar('\n');
	return (ret + 1);
}
