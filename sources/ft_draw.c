/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:38:55 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 16:38:55 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

void	draw_ball(void)
{
	double	angle;

	angle = 0.f;
	glColor3f(0.561f, 0.024f, 0.58f);
	glVertex3f(g_ame.ball.x1, g_ame.ball.y1, 0.f);
	glBegin(GL_POLYGON);
	while (angle < TAU)
	{
		g_ame.ball.x2 = g_ame.ball.x1 + ft_cosinus(angle) * g_ame.ball.radius;
		g_ame.ball.y2 = g_ame.ball.y1 + ft_sinus(angle) * g_ame.ball.radius;
		glVertex3f(g_ame.ball.x2, g_ame.ball.y2, 0.f);
		angle += TAU / 24;
	}
	glEnd();
}

void	draw_block(t_block *block)
{
	if (block)
	{
		glBegin(GL_POLYGON);
		if (block->d == 1)
			glColor3f(0.6f, 0.f, 0.f);
		else if (block->d == 2)
			glColor3f(0.4f, 0.f, 0.f);
		else if (block->d == 3)
			glColor3f(0.2f, 0.f, 0.f);
		else if (block->d == -1)
			glColor3f(0.169f, 0.204f, 0.267f);
		glVertex3d(block->x1, block->y1, 0.f);
		glVertex3d(block->x1, block->y2, 0.f);
		glVertex3d(block->x2, block->y2, 0.f);
		glVertex3d(block->x2, block->y1, 0.f);
		glEnd();
	}
}
