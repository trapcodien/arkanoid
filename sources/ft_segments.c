/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_segments.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:39:08 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 16:39:08 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

void	print_segments(int position, char *state)
{
	int	i;

	i = 0;
	while (state[i])
	{
		if (state[i] == 'A')
			print_segment_a(position + 1);
		else if (state[i] == 'B')
			print_segment_b(position + 1);
		else if (state[i] == 'C')
			print_segment_c(position + 1);
		else if (state[i] == 'D')
			print_segment_d(position + 1);
		else if (state[i] == 'E')
			print_segment_e(position + 1);
		else if (state[i] == 'F')
			print_segment_f(position + 1);
		else if (state[i] == 'G')
			print_segment_g(position + 1);
		i++;
	}
}

void	print_segment_a(int position)
{
	float	posx;
	float	posy;
	float	sizex;
	float	sizey;

	sizex = 0.03f;
	sizey = 0.01f;
	posx = -g_ame.ratio + (position * (sizex + sizey));
	posy = 0.99f;
	glColor3f(1.f, 1.f, 1.f);
	glRectf(posx, posy, posx - sizex, posy - sizey);
}

void	print_segment_b(int position)
{
	float	posx;
	float	posy;
	float	sizex;
	float	sizey;

	sizex = 0.01f;
	sizey = 0.03f;
	posx = -g_ame.ratio + (position * (sizex + sizey));
	posy = 0.99f;
	glColor3f(1.f, 1.f, 1.f);
	glRectf(posx, posy, posx - sizex, posy - sizey);
}

void	print_segment_c(int position)
{
	float	posx;
	float	posy;
	float	sizex;
	float	sizey;

	sizex = 0.01f;
	sizey = 0.03f;
	posx = -g_ame.ratio + (position * (sizex + sizey));
	posy = 0.99f - (sizey - sizex);
	glColor3f(1.f, 1.f, 1.f);
	glRectf(posx, posy, posx - sizex, posy - sizey);
}

void	print_segment_d(int position)
{
	float	posx;
	float	posy;
	float	sizex;
	float	sizey;

	sizex = 0.03f;
	sizey = 0.01f;
	posx = -g_ame.ratio + (position * (sizex + sizey));
	posy = 0.99f - (sizey * 4);
	glColor3f(1.f, 1.f, 1.f);
	glRectf(posx, posy, posx - sizex, posy - sizey);
}
