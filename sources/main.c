/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:39:11 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 22:26:25 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

t_game	g_ame;

static void		key_callback(
	GLFWwindow *window,
	int key,
	int scancode,
	int action,
	int mods)
{
	(void)scancode;
	(void)mods;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
	else if (key == GLFW_KEY_LEFT && action == GLFW_PRESS)
		g_ame.left = TRUE;
	else if (key == GLFW_KEY_LEFT && action == GLFW_RELEASE)
		g_ame.left = FALSE;
	else if (key == GLFW_KEY_RIGHT && action == GLFW_PRESS)
		g_ame.right = TRUE;
	else if (key == GLFW_KEY_RIGHT && action == GLFW_RELEASE)
		g_ame.right = FALSE;
	else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		g_ame.ball.speed = 3.f;
}

static void		ft_loop(GLFWwindow *window)
{
	if (!select_map())
		glfwSetWindowShouldClose(window, GL_TRUE);
	glfwSetWindowSize(window, WIDTH, HEIGHT);
	ft_update(window);
	ft_render(window);
	glfwSwapBuffers(window);
	glfwPollEvents();
}

int				main(void)
{
	GLFWwindow	*window;

	if (!game_init())
		return (ft_error("Error: Unable to init GLFX."));
	window = glfwCreateWindow(WIDTH, HEIGHT, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return (1);
	}
	glfwMakeContextCurrent(window);
	glfwSetKeyCallback(window, key_callback);
	while (!glfwWindowShouldClose(window))
		ft_loop(window);
	ft_printf("%s : %d\n", "Votre score", g_ame.score);
	glfwDestroyWindow(window);
	glfwTerminate();
	return (0);
}
