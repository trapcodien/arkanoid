/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:38:57 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 16:38:58 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

static void		error_callback(int error, const char *description)
{
	(void)error;
	ft_error(description);
}

void			ft_reset_ball(void)
{
	g_ame.ball.x1 = g_ame.pad.x1 + (g_ame.pad.x2 - g_ame.pad.x1) / 2;
	g_ame.ball.y1 = -0.87f;
	g_ame.ball.dir_x = 0.f;
	g_ame.ball.dir_y = 1.f;
	g_ame.ball.radius = 0.03f;
	g_ame.ball.speed = 0.f;
}

t_bool			game_init(void)
{
	ft_bzero(&g_ame, sizeof(t_game));
	g_ame.ratio = WIDTH / (float)HEIGHT;
	load_maps();
	if (g_ame.maps)
	{
		g_ame.nb_blocks = g_ame.maps->nb_blocks;
		g_ame.blocks = g_ame.maps->data;
	}
	g_ame.score = 0;
	g_ame.lives = 3;
	g_ame.pad.x1 = -0.25f;
	g_ame.pad.y1 = -0.9f;
	g_ame.pad.x2 = 0.25f;
	g_ame.pad.y2 = -0.95f;
	ft_reset_ball();
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		return (FALSE);
	return (TRUE);
}
