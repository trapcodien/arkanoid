/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_loader.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:39:00 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 16:39:00 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

static t_bool		valid_line(char *line)
{
	int				i;

	i = 0;
	while (line[i])
	{
		if (line[i] != ' ' && !ft_isdigit(line[i]) && line[i] != '*')
			return (FALSE);
		i++;
	}
	return (TRUE);
}

t_bool				load_blockfile(char *filename)
{
	int				fd;
	char			*line;
	int				line_num;
	int				ret;

	g_ame.nb_blocks = 0;
	if ((fd = open(filename, O_RDONLY)) == ERROR)
		return (FALSE);
	line_num = 0;
	while (line_num < 12 && (ret = get_next_line(fd, &line)) == 1)
	{
		if (!valid_line(line))
		{
			ft_strdel(&line);
			close(fd);
			return (FALSE);
		}
		add_blockline(line, line_num);
		ft_strdel(&line);
		line_num++;
	}
	close(fd);
	if (ret == ERROR)
		return (FALSE);
	return (TRUE);
}

t_bool				select_map(void)
{
	if (!g_ame.nb_blocks)
	{
		erase_blocks();
		list_del(&(g_ame.maps), g_ame.maps, &ft_memdel);
		if (g_ame.maps)
		{
			g_ame.blocks = g_ame.maps->data;
			g_ame.nb_blocks = g_ame.maps->nb_blocks;
			ft_reset_ball();
		}
		else
			return (FALSE);
	}
	return (TRUE);
}

void				load_maps(void)
{
	DIR				*d;
	struct dirent	*entry;
	char			*path;

	if (!(d = opendir("levels")))
	{
		ft_fprintf (2, "Cannot open directory '%s': %s\n",
				"levels", strerror (errno));
		exit(EXIT_FAILURE);
	}
	while ((entry = readdir(d)))
	{
		if (ft_strnequ(entry->d_name, ".") && ft_strnequ(entry->d_name, ".."))
		{
			list_add(&(g_ame.maps), ft_memalloc(sizeof(t_map)));
			if (!load_blockfile(path = ft_strjoin("levels/", entry->d_name)))
				ft_fatal("Map error.");
			ft_strdel(&path);
			g_ame.maps->data = g_ame.blocks;
			g_ame.maps->nb_blocks = g_ame.nb_blocks;
			g_ame.blocks = NULL;
		}
	}
	closedir(d);
}
