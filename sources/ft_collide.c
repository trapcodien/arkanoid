/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_collide.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:38:53 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 16:38:54 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

#include <stdio.h>

static void	ft_bounce_ball_on_pad_left(float half_pad)
{
	if (g_ame.ball.x1 < g_ame.pad.x1 + half_pad / 4 && g_ame.ball.dir_x > 0)
	{
		g_ame.ball.dir_y = -g_ame.ball.dir_y;
		g_ame.ball.dir_x = -g_ame.ball.dir_x;
	}
	else
	{
		g_ame.ball.dir_y = -g_ame.ball.dir_y;
		g_ame.ball.dir_x -= 0.3f;
	}
}

static void	ft_bounce_ball_on_pad_right(float half_pad)
{
	if (g_ame.ball.x1 > g_ame.pad.x1 + half_pad + 3 * half_pad / 4 &&
		g_ame.ball.dir_x < 0)
	{
		g_ame.ball.dir_y = -g_ame.ball.dir_y;
		g_ame.ball.dir_x = -g_ame.ball.dir_x;
	}
	else
	{
		g_ame.ball.dir_y = -g_ame.ball.dir_y;
		g_ame.ball.dir_x += 0.3f;
	}
}

void		ft_collision_pad_ball(void)
{
	float	half_pad;

	if (g_ame.ball.x1 + g_ame.ball.radius > g_ame.pad.x1 &&
		g_ame.ball.x1 - g_ame.ball.radius < g_ame.pad.x2 &&
		g_ame.ball.y1 - g_ame.ball.radius < g_ame.pad.y1)
	{
		half_pad = (g_ame.pad.x2 - g_ame.pad.x1) / 2;
		if (g_ame.ball.x1 < g_ame.pad.x1 + half_pad)
			ft_bounce_ball_on_pad_left(half_pad);
		else
			ft_bounce_ball_on_pad_right(half_pad);
	}
}

static void	ft_collide_side_block(t_block *curs)
{
	if (g_ame.ball.x1 - g_ame.ball.radius < curs->x1 &&
		g_ame.ball.x1 + g_ame.ball.radius > curs->x2 &&
		(g_ame.ball.y1 + g_ame.ball.radius > curs->y2 ||
		g_ame.ball.y1 - g_ame.ball.radius < curs->y1))
	{
		if (g_ame.ball.y1 < curs->y2 || g_ame.ball.y1 > curs->y1)
			g_ame.ball.dir_y = -g_ame.ball.dir_y;
		else
			g_ame.ball.dir_x = -g_ame.ball.dir_x;
	}
	if (curs->d != -1)
		curs->d--;
	if (curs->d == 0)
	{
		g_ame.score += 10;
		g_ame.nb_blocks--;
		list_del(&g_ame.blocks, curs, &ft_memdel);
	}
}

void		ft_collision_block_ball(void)
{
	t_block		*curs;

	curs = g_ame.blocks;
	while (curs)
	{
		if (g_ame.ball.x1 - g_ame.ball.radius < curs->x1 &&
			g_ame.ball.x1 + g_ame.ball.radius > curs->x2 &&
			g_ame.ball.y1 - g_ame.ball.radius < curs->y1 &&
			g_ame.ball.y1 + g_ame.ball.radius > curs->y2)
		{
			ft_collide_side_block(curs);
			return ;
		}
		curs = curs->next;
	}
}
