/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_maths.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:39:02 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 16:39:03 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

static float	ft_pow(float val, int power)
{
	int		i;
	float	ret_val;

	i = 1;
	ret_val = 1;
	while (i <= power)
	{
		ret_val *= val;
		i++;
	}
	return (ret_val);
}

static float	ft_fact(int x)
{
	int		i;
	float	ret_val;

	i = 1;
	ret_val = 1;
	while (i <= x)
	{
		ret_val *= i;
		i++;
	}
	return (ret_val);
}

float			ft_cosinus(float x)
{
	float	p;
	int		c;
	int		n;
	float	cos_val;

	p = 0.001;
	c = 1;
	n = -2;
	cos_val = 0;
	while ((ft_pow(x, n + 1) / ft_fact(n + 1)) >= p)
	{
		cos_val = c * (ft_pow(x, n + 2) / ft_fact(n + 2)) + cos_val;
		c = -1 * c;
		n += 2;
	}
	return (cos_val);
}

float			ft_sinus(float x)
{
	return (ft_cosinus(PI / 2 + x));
}
