/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_score.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:39:03 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 21:58:26 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

void			print_digit(int position, char digit)
{
	if (digit == '0')
		print_segments(position, "ABCFED");
	else if (digit == '1')
		print_segments(position, "BC");
	else if (digit == '2')
		print_segments(position, "ABEGD");
	else if (digit == '3')
		print_segments(position, "ABCDG");
	else if (digit == '4')
		print_segments(position, "FGBC");
	else if (digit == '5')
		print_segments(position, "AFGCD");
	else if (digit == '6')
		print_segments(position, "ACDEFG");
	else if (digit == '7')
		print_segments(position, "ABC");
	else if (digit == '8')
		print_segments(position, "ABCDEFG");
	else if (digit == '9')
		print_segments(position, "ABCFG");
}

void			draw_score(void)
{
	char	*str;
	int		i;

	i = 0;
	str = ft_itoa(g_ame.score);
	while (str[i])
	{
		print_digit(i, str[i]);
		i++;
	}
}
