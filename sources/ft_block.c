/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_block.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:38:51 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 16:38:51 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

static t_block	*create_block(int x, int y, int life)
{
	float		sizex;
	float		sizey;
	float		posx;
	float		posy;
	t_block		*b;

	if (x < 0 || y < 0 || x >= 12 || y >= 12 || life == 0)
		return (NULL);
	if (!(b = ft_memalloc(sizeof(t_block))))
		return (NULL);
	sizex = 0.22f;
	sizey = 0.12f;
	posx = g_ame.ratio - (x * sizex);
	posy = 1.f - (y * sizey);
	b->x1 = posx - 0.02f;
	b->y1 = posy - 0.02f;
	b->x2 = posx - sizex;
	b->y2 = posy - sizey;
	b->d = life;
	return (b);
}

void			add_blockline(char *line, int line_num)
{
	size_t	i;
	int		d;

	if (line_num >= 12)
		return ;
	i = 0;
	while (line[i] && i < 12)
	{
		d = line[i] - '0';
		if (line[i] == '*')
			list_add(&(g_ame.blocks), create_block(11 - i, line_num, -1));
		else if (ft_isdigit(line[i]) && line[i] != '0')
		{
			list_add(&(g_ame.blocks), create_block(11 - i, line_num, d));
			g_ame.nb_blocks++;
		}
		i++;
	}
}

void			draw_blocks(void)
{
	t_block	*cursor;

	cursor = g_ame.blocks;
	while (cursor)
	{
		draw_block(cursor);
		cursor = cursor->next;
	}
}

void			erase_blocks(void)
{
	g_ame.nb_blocks = 0;
	while (g_ame.blocks)
		list_del(&(g_ame.blocks), g_ame.blocks, &ft_memdel);
}
