/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_update.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jvincent <jvincent@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 19:48:52 by jvincent          #+#    #+#             */
/*   Updated: 2015/05/03 22:31:41 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

static void		ft_update_pad_position(void)
{
	if (g_ame.left && g_ame.pad.x2 >
			-g_ame.ratio + (g_ame.pad.x2 - g_ame.pad.x1))
	{
		g_ame.pad.x1 -= 0.04f;
		g_ame.pad.x2 -= 0.04f;
		if (g_ame.ball.speed == 0)
			g_ame.ball.x1 = g_ame.pad.x1 + (g_ame.pad.x2 - g_ame.pad.x1) / 2;
	}
	if (g_ame.right && g_ame.pad.x1 <
			g_ame.ratio - (g_ame.pad.x2 - g_ame.pad.x1))
	{
		g_ame.pad.x1 += 0.04f;
		g_ame.pad.x2 += 0.04f;
		if (g_ame.ball.speed == 0)
			g_ame.ball.x1 = g_ame.pad.x1 + (g_ame.pad.x2 - g_ame.pad.x1) / 2;
	}
}

void			ft_update(GLFWwindow *window)
{
	ft_update_pad_position();
	g_ame.ball.x1 += g_ame.ball.speed * g_ame.ball.dir_x * 0.01;
	g_ame.ball.y1 += g_ame.ball.speed * g_ame.ball.dir_y * 0.01;
	if (g_ame.ball.x1 > g_ame.ratio - g_ame.ball.radius)
		g_ame.ball.dir_x = -g_ame.ball.dir_x;
	else if (g_ame.ball.x1 < -g_ame.ratio + g_ame.ball.radius)
		g_ame.ball.dir_x = -g_ame.ball.dir_x;
	if (g_ame.ball.y1 > 1.00 - g_ame.ball.radius)
		g_ame.ball.dir_y = -g_ame.ball.dir_y;
	else if (g_ame.ball.y1 < g_ame.pad.y1)
	{
		g_ame.lives--;
		if (!g_ame.lives)
			glfwSetWindowShouldClose(window, GL_TRUE);
		ft_reset_ball();
	}
	ft_collision_pad_ball();
	ft_collision_block_ball();
}

void			ft_render(GLFWwindow *window)
{
	int		width;
	int		height;

	glfwGetFramebufferSize(window, &width, &height);
	g_ame.ratio = width / (float)height;
	glViewport(0, 0, width, height);
	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-g_ame.ratio, g_ame.ratio, -1.f, 1.f, 1.f, -1.f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor3f(0.035f, 0.341f, 0.863f);
	glRectf(g_ame.pad.x1, g_ame.pad.y1, g_ame.pad.x2, g_ame.pad.y2);
	draw_ball();
	draw_blocks();
	draw_score();
	draw_life();
}
