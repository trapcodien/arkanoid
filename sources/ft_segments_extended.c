/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_segments_extended.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/03 16:39:09 by garm              #+#    #+#             */
/*   Updated: 2015/05/03 16:39:10 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "arkanoid.h"

void	print_segment_e(int position)
{
	float	posx;
	float	posy;
	float	sizex;
	float	sizey;

	sizex = 0.01f;
	sizey = 0.03f;
	posx = (-g_ame.ratio + (position * (sizex + sizey)) - (sizex * 2));
	posy = 0.99f - (sizey - sizex);
	glColor3f(1.f, 1.f, 1.f);
	glRectf(posx, posy, posx - sizex, posy - sizey);
}

void	print_segment_f(int position)
{
	float	posx;
	float	posy;
	float	sizex;
	float	sizey;

	sizex = 0.01f;
	sizey = 0.03f;
	posx = (-g_ame.ratio + (position * (sizex + sizey)) - (sizex * 2));
	posy = 0.99f;
	glColor3f(1.f, 1.f, 1.f);
	glRectf(posx, posy, posx - sizex, posy - sizey);
}

void	print_segment_g(int position)
{
	float	posx;
	float	posy;
	float	sizex;
	float	sizey;

	sizex = 0.03f;
	sizey = 0.01f;
	posx = -g_ame.ratio + (position * (sizex + sizey));
	posy = 0.99f - (sizey * 2);
	glColor3f(1.f, 1.f, 1.f);
	glRectf(posx, posy, posx - sizex, posy - sizey);
}
